﻿using CovidApp.Models;
using CovidApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CovidApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        public SearchViewModel SearchViewModel { get; set; }

        public SearchPage()
        {
            InitializeComponent();
            BindingContext = this.SearchViewModel = new SearchViewModel();
        }

        public async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (CountryModel)layout.BindingContext;
            await Navigation.PushAsync(new DetailPage(new CovidDetailsViewModel(item)));
        }

        public void SearchBar_TextChanged(object sender, TextChangedEventArgs args)
        {
            var _container = BindingContext as SearchViewModel;
            CountryListView.BeginRefresh();

            if (string.IsNullOrEmpty(args.NewTextValue))
            {
                CountryListView.ItemsSource = _container.CountryModel;
            }
            else
            {
                CountryListView.ItemsSource = _container.CountryModel.Where(x => x.Country.Contains(args.NewTextValue));
            }

            CountryListView.EndRefresh();
        }
    }
}