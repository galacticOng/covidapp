﻿using CovidApp.Models;
using CovidApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CovidApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        private CovidDetailsViewModel covidDetailsViewModel;

        public Position Position { get; set; }

        public DetailPage()
        {
            InitializeComponent();
        }

        public DetailPage(CovidDetailsViewModel covidDetailsViewModel)
        {
            InitializeComponent();
            BindingContext = this.covidDetailsViewModel = covidDetailsViewModel;
        }
    }
}