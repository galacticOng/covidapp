﻿using CovidApp.Models;
using CovidApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Microcharts.Forms;
using Microcharts;
using Entry = Microcharts.Entry;

namespace CovidApp.ViewModels
{
    public class CovidDetailsViewModel : BaseViewModel
    {
        CovidServices covidService = new CovidServices();
        public CountryModel CountryModel { get; set; }

        public List<CountryDetailModel> CountryDetailModel { get; set; }

        public Position Position { get; set; }

        public CovidDetailsViewModel(CountryModel country)
        {
            try
            {
                IsBusy = true;
                CountryModel = country;
                GetDetailsData(CountryModel.Country);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async void GetDetailsData(string country)
        {
            var data = await covidService.GetCovidCountryDetailData(CountryModel.Country);
            CountryDetailModel = data;
            var indexCount = CountryDetailModel.Count - 1;
            Position = new Position(CountryDetailModel[indexCount].Lat, CountryDetailModel[indexCount].Lon);
        }
    }
}
