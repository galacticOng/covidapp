﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CovidApp.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public string TitleText { get; }
        public string AboutText { get; }
        public AboutViewModel()
        {
            Title = "About";
            TitleText = "What is Covid-19";
            AboutText = "COVID-19 is the infectious disease caused by the most recently discovered corona virus. This new virus and disease were unknown before the outbreak began in Wuhan, China, in December 2019.";
            GetSymptomsInfo();
            GetPreventionsInfo();
        }

        private void GetSymptomsInfo()
        {
            SymptomsInfoCollection = new ObservableCollection<AboutInfo>();
            SymptomsInfoCollection.Add(new AboutInfo() { Name = "Fever", Image = "fever.png" });
            SymptomsInfoCollection.Add(new AboutInfo() { Name = "Cough", Image = "coughing.png" });
            SymptomsInfoCollection.Add(new AboutInfo() { Name = "Sore Troath", Image = "troath.png" });
        }

        private void GetPreventionsInfo()
        {
            PreventionsInfoCollection = new ObservableCollection<AboutInfo>();
            PreventionsInfoCollection.Add(new AboutInfo() { Name = "Wear Mask", Image = "protection_mask.png" });
            PreventionsInfoCollection.Add(new AboutInfo() { Name = "Face Off", Image = "notouch_face.png" });
        }

        public ObservableCollection<AboutInfo> SymptomsInfoCollection { get; private set; }
        public ObservableCollection<AboutInfo> PreventionsInfoCollection { get; private set; }
    }

    public class AboutInfo
    {
        public string Name { get; set; }
        public string Image { get; set; }
    }
}