﻿using CovidApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.Services
{
    public interface ICovidServices
    {
        Task<CovidModel> GetCovidGlobalData();

        Task<List<CountryDetailModel>> GetCovidCountryDetailData(string country);
    }
}
